#!/bin/bash
# 1_fib.sh: Usage 1_fib.sh int_fib
# Evaluate fibonacci of the first command line parameter, check for a correct input
if [[ $# -lt 1 ]]
then
    echo "Too Few Arguments Provided"
    echo "Usgae $(basename $0) int_fib"
    exit 
elif [[ $# -gt 1 ]] 
then
    echo "Number of Arguments large"
    echo "Usgae $(basename $0) int_fib"
    exit
fi
case $1 in 
    [0-4][0-9] ) ;; #Values between 10-49   
    [0-9] ) ;;   #Single digit values
    50 ) ;;  
    #/^[0-4][0-9]$/ ) ;; # You cannot do a regex match in case
    * ) echo "Invalid Input Type, input a value less than 50"  # Checks both string input and whenever the input value is larger than 50
        echo "Usgae $(basename $0) int_fib" 
        exit 
        ;;
esac

function fib(){
    param=$1
    if [[ $param -eq 0 ]] || [[ $param -eq 1 ]]
    then
        echo -n "$param"
    else
        echo -n "$(( $( fib $(($param-1)) ) + $( fib $(($param-2)) ) ))"
    fi
}
# Return the fibonacci number recursively

fib $1 #Evaluate fibonacci for the first commandline parameter
echo

