#!/bin/bash
# 2_patterns.sh prints patterns based on the user input
E_NOARGS=200 # Error code for incorrect arguments (arbitrary)
E_OPTION=300 # Error code for selecting incorrect option (arbitrary)
E_NAN=400    # Error code for not selecting a number (arbitrary)
#=================================================================================================================#

function check_args(){
    for args in $@
    do
        if [[ ! $args =~  ^[0-9]*$ ]]
        then
            echo "Please provide positive numeric parameters"
            exit $E_NAN
        fi
    done             
} # Check whether the argument provided is a number

#=================================================================================================================#

function print_rect(){
    echo
    if [[ $# -ne 2 ]]
    then
        echo "Please provide exactly two parameters for rows and columns"
        exit $E_NOARGS
    fi
    check_args $@
    rows=$1
    columns=$2
    for (( i=1; i<=$rows; i++ ))
    do
        for (( j=0; j<$columns; j++ )) 
        do
            echo -n "* " #Executes ($1 * $2) times
        done
        echo
    done
    echo
} # Print a rectangle provided the number of rows and columns

#=================================================================================================================#

function print_rtriangle(){
    echo
    if [[ $# -ne 1 ]]
    then
        echo "Please provide exactly one argument for number of rows in the triangle"
        exit $E_NOARGS
    fi
    check_args $@
    rows=$1
    for (( i=1;i<=$rows;i++ ))
    do
        for (( j=1; j<=$i; j++ ))
        do
            echo -n "* "
        done
        echo
    done
    echo
} # Print a right triangle provided the number of rows

#=================================================================================================================#

function print_ftriangle(){
    echo
    if [[ $# -ne 1 ]]
    then
        echo "Please provide exactly one argument for number of rows in the floyd's triangle"
        exit $E_NOARGS
    fi
    check_args $@
    rows=$1
    print_val=0
    for (( i=1;i<=$rows;i++ ))
    do
        for (( j=1; j<=$i; j++ ))
        do
            (( ++print_val ))
            echo -n "$print_val "
        done
        echo
    done
    echo
} # Print a floyd's triangle provided the number of rows

#=================================================================================================================#

if [[ $# -lt 1 ]]
then
    echo "Please Provide correct number of arguments"
    echo -e "Usage: \n ./`basename $0` -r nows ncolumns (rectangle nrows * ncolumns)"
    echo -e " ./`basename $0` -t nrows (right triangle with nrows number of rows)" 
    echo -e " ./`basename $0` -f nrows (floyd's triangle with nrows number of rows)"
    exit $E_NOARGS

fi

case $1 in
    -r ) shift # shift the command line arguments by 1
         print_rect $@
         ;;        
    -t ) shift 
         print_rtriangle $@
         ;;     
    -f ) shift
         print_ftriangle $@
         ;;
    *  ) echo "Invalid Option Selected"
         echo -e "Usage: \n ./`basename $0` -r rows columns (rectangle row * column)"
         echo -e " ./`basename $0` -t nrows (right triangle with nrows number of rows)" 
         echo -e " ./`basename $0` -f nrows (floyd's triangle with nrows number of rows)"
         ;;
esac
