#include<stdio.h>
/* Print the first 20 fibonacci numbers */
int fib(int n){
    int ret;
    if(n==0 || n == 1){
        return 1;
    }
    else{
        ret = fib(n-1)+fib(n-2);
        return ret;
    } 
}// Recursive call

void fib2(int n){
    int i, fp=0,fpp=0, fc=0;
    for(i = 0; i<n; i++){ 
        if(i==0 || i==1){
            fc=fpp=fp=1;
        }//the first two terms are 1.
        else{
            fc=fpp+fp;
            fpp=fp;
            fp=fc;
        }// current term is the sum of previous two terms
        printf("%d\t", fp);
    }
    printf("\n");
}// iterative call

int main(){
    int n=0; 
    printf("\n");
//    fib2(20);
    
    while(n!=20){
        printf("%d\t",fib(n));
        n++;
    }
    printf("\n"); 
    return 0;
}
