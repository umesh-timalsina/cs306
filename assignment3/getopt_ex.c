#include<ctype.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc, char **argv){
    int aflag = 0;
    int bflag = 0;
    int cflag = 0;
    char *cvalue = NULL;
    int index;
    int c;
    opterr = 0;
    while((c = getopt(argc, argv, "abc:")) != -1){
        switch(c){
        case 'a':
            aflag = 1;
            break;
        case 'b':
            bflag = 1;
        case 'c':
            cflag = 1;
        case '?':
            if(optopt == 'c')
                fprintf(stderr, "Option %c requires an argument.\n", optopt);
            else if(isprint(optopt))
                fprintf(stderr, "Unknown option %c.\n", optopt);
            else
                fprintf(stderr, "Unknown character `\\x%x`.\n", optopt);
        }
    }// end while
}// end main
