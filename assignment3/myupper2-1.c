#include<stdio.h>
#include<stdlib.h>


int len(char* str){
    int len = 0;
    while(str[len] != '\0'){
        len++;
    }// end while
    return len;
}// end len

/* to_upper : convert source string into upper case*/
void to_upper(char *src, char *dest){
    
    int i, length = len(src);
    for(i = 0; i < length; i++){
        *(dest+i) = (*(src+i) >= 97 && *(src+i) <= 122 )? *(src+i)-32 : *(src+i);
    }// end for
    *(dest+i) = '\0';
}// end to_upper

/* to_upper_string : convert an array of strings into upper case */
void to_upper_string(char *src[], char *dest[], int n){
    int i;
    for(i = 0; i < n; i++){
        to_upper(src[i], dest[i]);
    }// end for
    dest[i] = NULL;
}// end to_upper_string

/* print_list : print a list of array of strings */
void print_list(char *data[]){
  int i = 0;
  while(data[i] != NULL){
    printf("%s ", data[i]);
    i++;
  }// end while
  printf("\n");
}// end print_list
