#include<stdio.h>
#include<stdlib.h>

/* main: Convert the series of command line strings into uppercase */
/* argv is the number of arguments passed, argc is the array of strings */

int main(int argv, char *argc[]){ 
    /* Function Prototypes */
    void to_upper(char *src, char *dest);
    void to_upper_string(char *src[], char *dest[], int lim);
    void print_list(char *data[]);
    int len(char *str);
    void print_list(char *data[]);
    /* end function prototypes */

    int i;    // iteration counters
    char **dest; // An array of character pointers
    
    /* Check for the command line arguments correctly */
    if(argv < 2){
        fprintf(stderr, "Error Usage %s strings \n", argc[0]);
        exit(EXIT_FAILURE);
    }// end if 
    
    /* Allocate Memory for a pointer to character pointer */
    dest = (char **) calloc(argv, sizeof(char *));
    if(dest == NULL){
        fprintf(stderr, "Error Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if

    /* Allocate Memory for each command line string */
    for(i = 0; i < argv-1; i++){
        dest[i] = (char *) calloc(len(argc[i+1]), sizeof(char)); // logical
        if(dest[i] == NULL){
            fprintf(stderr ,"Error- Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if
    }// end for
    dest[i] = NULL;     // One unallocated location that points to null
    to_upper_string(argc+1, dest, argv-1);    // argc[0] is the basename
    print_list(dest);    
    if(dest != NULL)    free(dest);     // free memory

    return 0;
}// end main

/* len: calculate the length of a string */
int len(char* str){
    int len = 0;
    while(str[len] != '\0'){
        len++;
    }// end while
    return len;
}// end len

/* to_upper : convert source string into upper case*/
void to_upper(char *src, char *dest){
    
    int i, length = len(src);
    for(i = 0; i < length; i++){
        *(dest+i) = (*(src+i) >= 97 && *(src+i) <= 122 )? *(src+i)-32 : *(src+i);
    }// end for
    *(dest+i) = '\0';
}// end to_upper

/* to_upper_string : convert an array of strings into upper case */
void to_upper_string(char *src[], char *dest[], int n){
    int i;
    for(i = 0; i < n; i++){
        to_upper(src[i], dest[i]);
    }// end for
    dest[i] = NULL;
}// end to_upper_string

/* print_list : print a list of array of strings */
void print_list(char *data[]){
  int i = 0;
  while(data[i] != NULL){
    printf("%s ", data[i]);
    i++;
  }// end while
  printf("\n");
}// end print_list
