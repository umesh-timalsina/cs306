#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
    char *val;
    struct node *next_node;
};

typedef struct linked_list{
    struct node *head_node;
    int number_of_entries;
};

int main(){
    /* function prototypes */
    void create_node( struct node * new_node, char *arr, struct node *next_node);
    void create_linked_list(struct linked_list *, struct node *);    
    void add_node(struct linked_list *list, struct node *new_node);    
    void print_nodes(struct linked_list *list);
    void remove_node(struct linked_list *list, char *str);

    /* Allocate Memory for a new node */
    struct node * new_node = (struct node *)calloc(1, sizeof(struct node));
    if(new_node == NULL){
        fprintf(stderr," Error- Could not allocate memory\n");
        exit(EXIT_FAILURE);
    }// end if
    char *arr = "Hello";
    create_node(new_node, arr, NULL);   // create the node
    
    /* Allocate memory for a linked list */
    struct linked_list *mylist = (struct linked_list *)calloc(1, sizeof(struct linked_list));
    if(mylist == NULL){
        fprintf(stderr," Error- Could not allocate memory\n");
        exit(EXIT_FAILURE); 
    }// end if
    create_linked_list(mylist, new_node);   // create a new linked list
    //printf("%s", (mylist->head_node)->val);


    /* Allocate Memory for another new node */
    struct node * new_node2 = (struct node *)calloc(1, sizeof(struct node));
    if(new_node2 == NULL){
        fprintf(stderr," Error- Could not allocate memory\n");
        exit(EXIT_FAILURE);
    }// end if

    char *arr2 = "World";
    create_node(new_node2, arr2, NULL);   // create the node
    add_node(mylist, new_node2);
    //printf("%s", mylist->head_node->val);
     
    /* Allocate Memory for another new node */
    struct node * new_node3 = (struct node *)calloc(1, sizeof(struct node));
    if(new_node3 == NULL){
        fprintf(stderr," Error- Could not allocate memory\n");
        exit(EXIT_FAILURE);
    }// end if

    char *arr3 = "My";
    create_node(new_node3, arr3, NULL);   // create the node
    add_node(mylist, new_node3);
    //printf("%s", mylist->head_node->val);
    
    //printf("%s", mylist->head_node->val);
    remove_node(mylist, "World");
    print_nodes(mylist);

    return 0;

}// end main

/* create_node: creates a new node structure with arr as value and next_node as next node pointer*/
void create_node( struct node *new_node, char *arr, struct node *next_node){
    new_node->val = arr;
    new_node->next_node = next_node;
}// end create_node

/*create_linked_list: creates a new linked list with new_head_node as the head node */
void create_linked_list(struct linked_list *new_list, struct node *new_head_node){
    new_list->head_node = new_head_node;
    new_list->number_of_entries = 1;
}// end create_linked_list

/* add_node: Adds a new node to the existing linked list list */
void add_node(struct linked_list *list, struct node *new_node){
    if(list->head_node == NULL){
        new_node->next_node = NULL;
    }// end if
    else{
        new_node->next_node = list->head_node;
    }// end else 
    list->head_node = new_node;
    list->number_of_entries++;
}// end add_node

/* print_nodes: prints all the node values for the linked list named list */
void print_nodes(struct linked_list *list){
    struct node *current_node = list->head_node;
    while(current_node != NULL){
        printf("%s\n", current_node->val);
        current_node = current_node->next_node;
    }
}// end print_nodes

/* remove_node : removes the nodes with the specified value(str) in the list */
void remove_node(struct linked_list *list, char *str){
    void get_refs(struct linked_list *list, char *str, struct node **cn, struct node **pn);
    struct node *cn, *pn;
    cn = (struct node *) malloc(sizeof(struct node));
    pn = (struct node *) malloc(sizeof(struct node));
    if(cn == NULL || pn == NULL){
        fprintf(stderr, "Error Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    get_refs(list, str, &cn, &pn); 
    if(pn == NULL && cn != NULL){
        list->head_node = cn->next_node;
    }// end if
    else if(cn != NULL && pn != NULL){
        pn->next_node = cn->next_node;
    }// end else if
}// end remove_node

/* get_refs : gets reference to node containing str and its previous node*/
void get_refs(struct linked_list *list, char *str, struct node **cn, struct node **pn){
    *cn = list->head_node;
    *pn = NULL;
    printf("%s", *(*(cn).val));
   // while(*cn != NULL){
     //   if(strcmp((*cn)->val, str) == 0){
       //     break;
        //}// end if
        //*pn = *cn;
        //(*cn) = *((cn)->next_node);
    //}// end while
}// end get_refs

