#include<stdio.h>
#include<stdlib.h>

/* main: Convert the command line string into uppercase */
int main(int argv, char *argc[]){ 
    void to_upper(char *src, char *dest);
    int len(char *str);
    char *dest;
    
    /* check if command line argument is provided correctly */
    if(argc[1] == NULL){
        fprintf(stderr,"Error- Usage %s lowercase string\n", argc[0]);
        exit(EXIT_FAILURE);
    }// end if
    /* Allocate Memory by using calloc
       Notice the space (+1) for '\0' */
    dest = (char *) calloc(len(argc[1]+1), sizeof(char)); 
    if(dest == NULL){
        fprintf(stderr, "Error- Unable to alocate memory");
        exit(EXIT_FAILURE);
    }// end if
    to_upper(argc[1], dest);
    printf("%s\n", dest);
    if(dest != NULL) free(dest); // deallocate the memory
    return 0;
}// end main

/* len: calculate the length of a string */
int len(char* str){
    int len = 0;
    while(str[len] != '\0'){
        len++;
    }// end while
    return len;
}// end len

/* to_upper : convert source string into upper case*/
void to_upper(char *src, char *dest){
    int i, length = len(src);
    for(i = 0; i < length; i++){
        *(dest+i) = (*(src+i) >= 97 && *(src+i) <= 122 )? *(src+i)-32 : *(src+i);
    }// end for
    *(dest+i) = '\0';
}
