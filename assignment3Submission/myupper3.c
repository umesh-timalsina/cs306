#include<stdio.h>
#include<stdlib.h>
#include<string.h>
/* the node structure */
typedef struct node{
    char *val;
    struct node *next_node;
}node;

/* the linked_list structure */
typedef struct linked_list{
    struct node *head_node;
    int number_of_entries;
}linked_list;

int main(int argv, char *argc[]){
    /* function prototypes */
    void create_node( struct node * new_node, char *arr, struct node *next_node);
    void create_linked_list(struct linked_list *, struct node *);    
    void add_node(struct linked_list *list, struct node *new_node);    
    void print_linked_list(struct linked_list *list);
    /* Defined in myupper2-1.c */
    void to_upper(char *src, char *dest);       
    void to_upper_string(char *src[], char *dest[], int lim); 
    void print_list(char *data[]);
    int len(char *str);
    void print_list(char *data[]);    
    /* end function prototypes */

    int i;    // iteration counters
    char **dest; // An array of character pointers
    
    /* Check for the command line arguments correctly */
    if(argv < 2){
        fprintf(stderr, "Error Usage %s strings \n", argc[0]);
        exit(EXIT_FAILURE);
    }// end if 
    
    /* Allocate Memory for a pointer to character pointer */
    dest = (char **) calloc(argv, sizeof(char *));
    if(dest == NULL){
        fprintf(stderr, "Error Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if

    /* Allocate Memory for each command line string */
    for(i = 0; i < argv-1; i++){
        dest[i] = (char *) calloc(len(argc[i+1]), sizeof(char)); // logical
        if(dest[i] == NULL){
            fprintf(stderr ,"Error- Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if
    }// end for
    dest[i] = NULL;     // One unallocated location that points to null
    to_upper_string(argc+1, dest, argv-1);    // argc[0] is the basename
    
    i = 0;
    /* Create a new linked list */
    /* part1: allocate memory */
    struct linked_list *new_list = (struct linked_list *)malloc(sizeof(struct linked_list));    
    if(new_list == NULL){
        fprintf(stderr, "Error Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    /*part2: create linked nodes */
    struct node *node_new;
    create_linked_list(new_list, NULL);
    while(dest[i] != NULL){
        node_new = (struct node *)malloc(sizeof(struct node));
        if(node_new == NULL){
            fprintf(stderr, "Error Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if 
        create_node(node_new, dest[i], NULL);// creates a new node
        add_node(new_list, node_new); // Adds new_node to the list
        i++;
    }// end while
    print_linked_list(new_list);
    if(new_list != NULL) free(new_list); // free memory
    if(node_new != NULL) free(node_new); // free memory
    if(dest != NULL)    free(dest);      // free memory
    return 0;

}// end main

/* create_node: creates a new node structure with arr as value and next_node as next node pointer*/
void create_node( struct node *new_node, char *arr, struct node *next_node){
    new_node->val = arr;
    new_node->next_node = next_node;
}// end create_node

/*create_linked_list: creates a new linked list with new_head_node as the head node */
void create_linked_list(struct linked_list *new_list, struct node *new_head_node){
    new_list->head_node = new_head_node;
    new_list->number_of_entries = 1;
}// end create_linked_list

/* add_node: Adds a new node to the existing linked list list */
void add_node(struct linked_list *list, struct node *new_node){
    if(list == NULL){
        new_node->next_node = NULL;
    }// end if
    else{
        new_node->next_node = list->head_node;
    }// end else 
    list->head_node = new_node;
    list->number_of_entries++;
}// end add_node

/* print_nodes: prints all the node values for the linked list named list */
void print_linked_list(struct linked_list *list){
    struct node *current_node = list->head_node;
    while(current_node != NULL){
        printf("%s ", current_node->val);
        current_node = current_node->next_node;
    }
    printf("\n");
}// end print_nodes











