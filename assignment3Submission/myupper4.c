#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAXCHAR 1000 // Any string is not more than 1000 characters long
/* the node structure */
typedef struct node{
    char *val;
    struct node *next_node;
}node;

/* the linked_list structure */
typedef struct linked_list{
    struct node *head_node;
    int number_of_entries;
}linked_list;

int main(int argv, char *argc[]){
    /* function prototypes */
    void create_node( struct node * new_node, char *arr, struct node *next_node);
    void create_linked_list(struct linked_list *, struct node *);    
    void add_node(struct linked_list *list, struct node *new_node);    
    void print_linked_list(struct linked_list *list);
    void add_to_last(struct linked_list *list, struct node *new_node);
    /* Defined in myupper2-1.c */
    void to_upper(char *src, char *dest);       
    void to_upper_string(char *src[], char *dest[], int lim); 
    void print_list(char *data[]);
    int len(char *str);
    void print_list(char *data[]);   
    /* end function prototypes */
    FILE *fp; // Create a new file pointer
    int i = 0; // iteration counter   
    fp = fopen(argc[1],"r");    // take the file name provided by command line
    if(fp == NULL){
        fprintf(stderr, "Error- Failed to open file");
        exit(EXIT_FAILURE);
    }//end if
    char arr[MAXCHAR];// An array to temporarily hold the tokens
    struct linked_list *new_list = (struct linked_list *)malloc(sizeof(struct linked_list ));
    if(new_list == NULL){
        fprintf(stderr, "Error- Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    struct node *node_new;      // declare a new node pointer
    create_linked_list(new_list, NULL);         // create the new linked list   
    /* Read the file tokens */
    char **dest;        // Final destination to store strings
    /* Count the number of tokens in the file */
    while(fscanf(fp, "%s", arr) == 1){
        i++;
    }// end while
    rewind(fp);         //Go to the start of the file
    /* Allocate memory for the strings arrays*/
    dest = (char **) calloc(i, sizeof(char *)); 
    if(dest == NULL){
        fprintf(stderr, "Error- Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    i = 0;      // iteration counter
    while(fscanf(fp,"%s", arr) == 1){
        /* Allocate memory for node to store current token */
        node_new = (struct node *)malloc(sizeof(struct node));
        if(node_new == NULL){
            fprintf(stderr, "Error- Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if
        dest[i] = (char *)malloc(strlen(arr)*sizeof(char));
        if(dest[i] == NULL){
            fprintf(stderr, "Error- Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if
        strcpy(dest[i], arr); 
        create_node(node_new, dest[i], NULL);
        add_to_last(new_list, node_new);
        i++;
    }//end while
    print_linked_list(new_list);
    /* Print the summary of all the operations */
}// end main

void create_node( struct node *new_node, char *arr, struct node *next_node_ptr){
    new_node->val = arr;
    new_node->next_node = next_node_ptr;
}// end create_node

/*create_linked_list: creates a new linked list with new_head_node as the head node */
void create_linked_list(struct linked_list *new_list, struct node *new_head_node){
    new_list->head_node = new_head_node;
    new_list->number_of_entries = 1;
}// end create_linked_list


/* add_node: Adds a new node to the existing linked list list */
void add_node(struct linked_list *list, struct node *new_node){
    if(list->head_node == NULL){
        new_node->next_node = NULL;
    }// end if
    else{
        new_node->next_node = list->head_node;
        list->number_of_entries++;
    }// end else 
    list->head_node = new_node;
}// end add_node

/* add_to_last: Adds a new node to the last of the existing linked list list */
void add_to_last(struct linked_list *list, struct node *new_node){
    struct node *current_node = list->head_node;    
    if(current_node == NULL){
        list->head_node = new_node;
    }// end if
    else{
            while(current_node->next_node != NULL){
                current_node = current_node->next_node;
            }// end while
        current_node->next_node = new_node;
    }// end else
}// end add_to_last

/* print_nodes: prints all the node values for the linked list named list */
void print_linked_list(struct linked_list *list){
    struct node *current_node = list->head_node;
    while(current_node->next_node != NULL){
        printf("%s --> ", current_node->val);
        current_node = current_node->next_node;
    }
    printf("%s", current_node->val);
    printf("\n");
}// end print_nodes

