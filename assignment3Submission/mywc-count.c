#include<stdio.h>
#include<stdlib.h>
#define IN 1
#define OUT 0
#define MAXLINE 10000 // A single line does not have more than 10000 characters
void find_counts(char *file_name, int counts[]){
    FILE *fp = fopen(file_name, "r");// open the file in read mode
    int state = OUT;
    int line_length, max_length=0;
    char *line_ptr = NULL;
    size_t n = 0;
    if(fp == NULL){
        fprintf(stderr, "Error- Failed to open file\n");
        exit(EXIT_FAILURE);
    }//end if
    fseek(fp, 0, SEEK_END);     // Go One Bytes After SEEK_END
    counts[0] = ftell(fp);    // bytes count
    int c;
    rewind(fp); // rewind the file pointer
    while((c = fgetc(fp)) != EOF){
        counts[1]++;    // characters count
        (c == '\n')?(counts[2]++):(counts[2] += 0); // lines count
        if(c == '\n' || c == ' ' || c == '\t'){
            state = OUT;    
        }// end if
        else if(state == OUT){
            state = IN;
            counts[3]++;// words count
        }// end else if
    }//end while
    rewind(fp); // rewind the file pointer
    while((line_length = 
           getline(&line_ptr, &n, fp)) != -1){
        (line_length>max_length)? (max_length = line_length) 
                                : (max_length = max_length);
    }// end while
    counts[4] = max_length-1;   // Remove '\n' delimeter to count line length
    if(line_ptr != NULL){
        free(line_ptr);
    }//end if
    if(fp != NULL){
        fclose(fp);
    }//end if
}// end find_counts
