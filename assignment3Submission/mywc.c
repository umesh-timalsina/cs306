#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>
#define MAXLINE 1000

typedef struct flags{
    unsigned int byte_flag            : 1;
    unsigned int char_flag            : 1;
    unsigned int line_flag            : 1;
    unsigned int max_line_length_flag : 1;
    unsigned int words_flag           : 1;
    unsigned int help_flag            : 1;
    unsigned int version_flag         : 1;
    char **input_file;
}flags;// defining options flag


int main(int argc, char **argv ){
    /*Define Function prototypes*/
    void find_option(struct flags *input_flag, 
                     int *num_files, 
                     int argc, char   **argv);
    void find_counts(char *file_name, int counts[]);    
    void print_with_opts(struct flags *flags_input, int counts[]);
    void display_version();
    void display_help();
    /* Define a structure to find the options selected */
    struct flags flag_input = {0, 0, 0, 0, 0, 0, 0, NULL};
    extern int opterr; 
    opterr = 0; // disable the error flag of the header file
    int counts[5] = {0, 0, 0, 0, 0};
    /* Define a pointer to the structure */
    struct flags *ptr;
    ptr = &flag_input;
    int num_files = 0; // The number of files to work on 
    /* call the function */
    find_option(ptr, &num_files, argc, argv);
    if(ptr->version_flag){
        display_version();
        exit(EXIT_SUCCESS);
    }// end if
    if(ptr->help_flag){
        display_help();
        exit(EXIT_SUCCESS);
    }// end if
    find_counts(flag_input.input_file[0], counts); // find the word count
//    printf("The number of bytes: %d characters: %d Lines: %d Words : %d maximum line: %d\n", 
  //                          counts[0], counts[1], counts[2], counts[3], counts[4]);
    print_with_opts(ptr, counts);
    free(ptr->input_file);
}// end main


void find_option(struct flags *input_flag, 
                 int *num_files,
                 int argc,        char   **argv){
        
    int c;
    while(1){
        int option_index = 0;
        static struct option long_options[] = {
            { "bytes",           no_argument,   NULL, 'c' },
            { "chars",           no_argument,   NULL, 'm' },
            { "lines",           no_argument,   NULL, 'l' },
            { "max-line-length", no_argument,   NULL, 'L' },
            { "words",           no_argument,   NULL, 'w' },
            { "help",            no_argument,   0,     0  },
            { "version",         no_argument,   0,     0  },
            { 0,                 0,             0,     0  },
        };// end long_options
        c = getopt_long(argc, argv, "cmlLw", 
                        long_options, &option_index);
        if(c == -1){
            break;
        }// break
        switch(c){
            case 0:
                //printf("%d\n", option_index);
                break;
            case 'c':
               // printf("%c\n", c);
                input_flag->byte_flag = 1;
                break;
            case 'm':
                input_flag->char_flag = 1;
                break;
            case 'l':
                input_flag->line_flag = 1;
                break;
            case 'L':
                input_flag->max_line_length_flag = 1;
                break;
            case 'w':
                input_flag->words_flag = 1;
                break;
            case '?':
                fprintf(stderr, "%s: invalid option -- '%s'\n", argv[0], argv[(optind-1)]);
                fprintf(stderr, "Try '%s --help' for more information.\n", argv[0]);
                exit(EXIT_FAILURE);
        }// end switch
        if(option_index == 5){
            input_flag->help_flag = 1;
        }// end if
        else if(option_index == 6){
            input_flag->version_flag = 1;
        }// end else if
    }// end while
    *num_files = argc-optind;
    int i  = 0;
    input_flag->input_file = (char **)malloc(sizeof(char *) * (*num_files));
    if(input_flag->input_file == NULL){
        fprintf(stderr, "Error- Failed to allocate memory \n");
        exit(EXIT_FAILURE);
    }//end if
    if(input_flag->input_file == NULL){
        fprintf(stderr, "Error- Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    for(i = 0; i < *num_files; i++){
        input_flag->input_file[i] = argv[optind+i];
    }// end for
}// end find_option

void print_with_opts(struct flags *flags_input, int counts[]){
   char str_to_print[MAXLINE]="";
   char buf[MAXLINE]="";
//   void display_help();
   void display_version();
   if(flags_input->line_flag){
        sprintf(buf, " %d ", counts[2]);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->words_flag){
       sprintf(buf, " %d ", counts[3]);
       strcat(str_to_print, buf);
   }//end if
   if(flags_input->byte_flag){
        sprintf(buf, " %d ", counts[0]);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->char_flag){
        sprintf(buf, " %d ", counts[1]);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->max_line_length_flag){
        sprintf(buf, " %d ", counts[4]);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->help_flag){
//        display_help();
        exit(EXIT_SUCCESS);
   }// end if
   if(flags_input->version_flag){
        // display_version(); 
        exit(EXIT_SUCCESS);
   }// end if
   if(strcmp(str_to_print, "") == 0){
        sprintf(str_to_print, " %d %d %d %s", 
                counts[2], counts[3], counts[0],flags_input->input_file[0]);
   }// end if
   else{
        strcat(str_to_print, flags_input->input_file[0]);
   }// end else
   printf("%s\n", str_to_print); 
}// end print_with_opts

void display_version(){
    printf(" mywc (Mimics wc) \n") ;
    printf("Copyright (C) 2017 Umesh Timalsina<umesh.timalsina@siu.edu> \n");
    printf("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n");
    printf("This is free software: you are free to change and redistribute it.\n");
    printf("There is NO WARRANTY, to the extent permitted by law.\n");
    printf( "Written by Umesh Timalsina \n");
}// end display_version

void display_help(){
    

  printf("usage: ./mywc [OPTION]... [FILE]...\n"
        "Print newline, word, and byte counts for a FILE.  A word is a non-zero-length sequence or\n"
         "characters delimited by white space.\n");

  printf("Some filename should be given, otherwise the program will exit by printing error message to\n"
         "stderr\n");

  printf("The options below may be used to select which counts are printed, always in\n"
           "the following order: newline, word, character, byte, maximum line length.\n"
           "-c, --bytes            print the byte counts\n"
           "-m, --chars            print the character counts\n"
           "-l, --lines            print the newline counts\n"
           "-L, --max-line-length  print the maximum display width\n"
           "-w, --words            print the word counts\n"
            "    --help             display this help and exit\n"
            "    --version          output version information and exit\n");


}// end display_help
