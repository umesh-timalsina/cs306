#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(int argc, char **argv){
    int i = 0; // iteration counter
    pid_t child_pids[argc-1]; // An array to hold the PIDs of child processes
    char *file_name;
    char *ps_argv[] = {"wc", "", 0};    // Arguments for execvp, empty for file name
    pid_t temp;
    int error_code;
    if(argc < 2){
        fprintf(stderr, "Error- Two few arguments\n");
        exit(EXIT_FAILURE);
    }// end if
    
    for(i = 0; i < (argc-1); i++){
        temp = fork();
        if(temp == -1){
            perror("Error- Failed to fork");
            exit(EXIT_FAILURE);
        }// end if
        else if(temp == 0){
            file_name = argv[i+1];
            break;
        }// promptly break out of the loop in child
        else{
            child_pids[i] = temp;
        }// end else
    }// end for
    
    if(temp == 0){
        ps_argv[1] = file_name;
        error_code = execvp("wc", ps_argv); // end execvp
        if(error_code == -1){
            perror("Error- Exec Failed\n");
        }// end if (code never actually reaches here)
    }// end if

    else{
        int stat_val[argc-1];
        int child_pid[argc-1];

        for(i = 0; i < argc-1; i++){
            child_pid[i] = waitpid(child_pids[i], &stat_val[i], 0);
            if(child_pid[i] == -1){
                fprintf(stderr, "Error-- Wait Failed\n");
                exit(EXIT_FAILURE);
            }// end if
            if(WIFEXITED(stat_val[i])){
                printf("Child with PID: %d Exited with code %d\n", 
                              child_pid[i], WEXITSTATUS(stat_val[i]));
            }// end if
            else{
                printf("Child with PID: %d Exited abnormally\n", 
                                                        child_pid[i]);
            }// end else
        }// end for
        printf("Finally, Every Child Has finished execution\n");  
    }// end else
    return 0;
}// end main

