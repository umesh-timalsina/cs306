#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<sys/stat.h>

int main(int argc, char **argv){
    if(argc != 3){
        fprintf(stderr, "Arguments Error");
        exit(EXIT_FAILURE);
    }// end if
    pid_t temp;
    /* Fork a child process */
    temp = fork();
    /*Handle Error related to fork*/
    if(temp == -1){
        perror("Failed to fork");
        exit(EXIT_FAILURE);
    }// end if
    
    /* Child Process */
    if(temp == 0)
    {
        int foptions, fd, fd_cp;
        mode_t fmode;
        foptions = O_WRONLY | O_CREAT;
        fmode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
        fd = open(argv[1], foptions, fmode);
        if(fd < 0){
            fprintf(stderr, "Error-- Failed to open file");
            exit(EXIT_FAILURE);
        }// end if
        /* Close the stdout */
        close(STDOUT_FILENO);
        /* Redirect stdout to file */
        fd_cp = dup(fd);
        if(fd_cp < 0){
            write(STDERR_FILENO,(void *) "Failed", 6);
            exit(EXIT_FAILURE);
        }//end if
        char *argv_exec[] = {"wget", "-qO-", argv[1], argv[2], 0};
        execvp("wget", argv_exec);
        /* No File Descriptors are closed 
           because there's no point in 
           closing as exec never returns */
    }// end if

    /* Parent Process */
    else{
        pid_t child_id;
        int exit_status;
        child_id = wait(&exit_status);
        if(WIFEXITED(exit_status)){
            printf("Child with pid %d exited with"
                    " Exit code %d\n", child_id,
                    WEXITSTATUS(exit_status));
        }// end if
        else{
            printf("Child with pid %d exited abnormally\n", child_id);
        }// end else
        exit(EXIT_SUCCESS);
    }// end else
    return 0;
}// end main

