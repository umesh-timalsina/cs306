#include<stdio.h>
#include<stdlib.h>

#define FIFO_FILE "MYFIFO"

int main(int argc, char **argv){
    FILE *fp;
    if(argc != 2){
        printf("USAGE: fifoclient.o [string]\n");
        exit(EXIT_FAILURE);
    }// end if

    if((fp = fopen(FIFO_FILE,"w")) == NULL){
        perror("fopen");
        exit(1);
    }// end if

    fputs(argv[1], fp);
    fclose(fp);

    return 0;
}// end main
