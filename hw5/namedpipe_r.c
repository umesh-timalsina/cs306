/* Your code goes here for including the user-defined namedpipe.h header file */

#include "namedpipe.h"
#include<signal.h>
/* Name and location of the named pipe on your filesystem */
#define FIFO_FILE       "./MY_FIFO"
int main(void)
{
	int fifo_fd, nbytes;
	packet_t packet;
    int res;   
    int open_mode = O_RDONLY;
	/* First, create the FIFO if it does not exist - your code goes here. Check access() system call on how to do it.*/
	if(access(FIFO_FILE, F_OK) == -1){
        res = mkfifo(FIFO_FILE, 0777);
        if(res != 0){
            fprintf(stderr, "Could not create FIFO %s\n", FIFO_FILE);
        }// end if
    }// end if

	/* If creating the named pipe was successful, now open it in READ_ONLY mode using system call open() - your code goes here */
	printf("Process %d Opening Pipe Receiver in read only...\n", getpid());
    fifo_fd = open(FIFO_FILE, open_mode);

	printf("\nNamed pipe receiver ready, waiting for packets...\n");
	do 
	{
		/* Read from the named pipe, your code goes here */
		read(fifo_fd, &packet, sizeof(packet));
		printf("Received from process %d data: %d\n", packet.senderID, packet.data);
		sleep(1);
		
		/* Send a SIGCONT signal to the sender process - your code goes here */
        kill(packet.senderID, SIGCONT);
	
	} while (packet.data > 0);
    
	/* Close the named pipe - your code goes here */    
    close(fifo_fd);
	
	printf("\nDone receiving data...\n\n");
	return 0;
}

	
