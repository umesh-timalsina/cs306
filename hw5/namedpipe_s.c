/* Your code goes here for including the user-defined namedpipe.h header file */
#include "namedpipe.h"
#include<signal.h>
#include<errno.h>
#include<string.h>
/* Name and location of your named pipe on filesystem */
#define FIFO_FILE       "./MY_FIFO"


int fifo_fd;

/* Write here the signal handler for SIGCONT */
void signal_handler(int sig){
    if(sig == SIGCONT){
        printf("Received SIGCONT signal\n");
    }// end if
    if(sig == SIGPIPE){
        printf("Received SIGPIPE signal\n");
    }// end if
    if(sig == SIGSTOP){
        printf("Received SIGSTOP signal\n");
    }// end if
}// end signal_handler

/* Write here the second signal handler for handling the "Broken Pipe" error message */
void broken_pipe_handler(int sig){
    if(sig == SIGPIPE){
        fprintf(stderr, "\n Broken Pipe Detected, error code= %d,\
                error message = %s\n", sig, strsignal(errno));
    }// end if
}// end broken_pipe_handler

int main(int argc, char *argv[])
{
		/* Your code goes here: First install the necessary signal handlers 
		 * You need to install TWO signal handlers correspoding 
		 * to the two signals (SIGCONT and SIGPIPE) you need to handle in your code 
		*/
        signal(SIGCONT, signal_handler);
        signal(SIGPIPE, broken_pipe_handler);
        signal(SIGSTOP, signal_handler);
		packet_t packet;
		int i, nwrite=0;
        int open_mode = O_WRONLY;
        int res;
		/* First open the named pipe in write-only mode, with standard error checking for open() system call */
		/* You need to create the named pipe too, if the opening of the named pipe fails. Check system call access
         * on how to use it for this case. (Do "man 2 access" for information about access() on a terminal)*/
        if(access(FIFO_FILE, F_OK) == -1){
            res = mkfifo(FIFO_FILE, 0777);
            if(res != 0){
                fprintf(stderr, "Could not create fifo %s\n", FIFO_FILE);
                exit(EXIT_FAILURE);
            }// end if
        }// end if

        printf("Process %d opening FIFO...", getpid());
        fifo_fd = open(FIFO_FILE, open_mode);
		packet.senderID = getpid();
		packet.data = 100;
		
		/* Now write the code for sending the above created packet to the receiver via the named pipe() */

		/* After every packet sent, raise a SIGSTOP to halt process and wait for receiver to send a SIGCONT to continue sending packets */
		/* Once, the sender receives a SIGCONT, continue sending packet inside the for loop */ 
        
		for(i = 5;i < 10;i++) {
			/* your SIGSTOP raising code goes here */

			packet.senderID = getpid();
			packet.data = i;
			
			res = write(fifo_fd, &packet, sizeof(packet));
            if(res == -1){
                fprintf(stderr, "Write error on pipe\n");
                exit(EXIT_FAILURE);
            }// end if
		    nwrite+=res;
			printf("Sent data = %d\n", packet.data);	
            raise(SIGSTOP);
		}// end for
	
		/* Indicate to receiver that all packets sent by sending a negative number. Receiver will close the read end of the pipe */
		for (i = 0;i < 5;i++) {
			packet.senderID = getpid();
			packet.data = -10;
			
			/* Your packet sending code goes here */
            res = write(fifo_fd, &packet, sizeof(packet));
            if(res == -1){
                fprintf(stderr, "Write Error on pipe");
                exit(EXIT_FAILURE);
            }// end if
			printf("Sent data = %d\n", packet.data);	
            
            /* if packet sending was unsuccessful, stop sending anymore packets, nwrite is the return from write system call which you have to write */
			if (nwrite < 0) break;		
			sleep(1);
		}
		
        /* your code goes here for closing the named pipe goes here */		
	    close(fifo_fd);

		printf("\nDone sending...\n\n");
        
		return 0;
}
