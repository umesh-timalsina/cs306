#include<stdio.h>
#include<stdlib.h>
#define MAXSTRS 5

int main(void){
    int cntr;
    FILE  *pipe_fp2;
    char buf[25];
    char *strings[MAXSTRS] = {"echo", "bravo", "alpha",
                               "charlie", "delta"};
    /* Create one way pipe line with call to popen */
    if((pipe_fp2 = popen("echo Hello", "r")) == NULL){
        perror("popen");
        exit(1);
    }// end if

    /* Processing Loop */
//   for(cntr=0; cntr<MAXSTRS; cntr++){
  //      fputs(strings[cntr], pipe_fp);
    //    fputc('\n', pipe_fp);
    //}// end for

    fgets(buf, 25, pipe_fp2);
    puts(buf);
    /* Close the pipe */
    pclose(pipe_fp2);
    return 0;

}// end main
