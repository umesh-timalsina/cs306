/*
 * Do not change anything in this file.
 */ 
#ifndef NAMEDPIPE_H_
	#include <stdio.h>
	#include <stdlib.h>
	#include <fcntl.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <sys/stat.h>
	#include <limits.h>
	#include <linux/stat.h>

	typedef struct {
		pid_t senderID;	/* process ID of the sender */
		int data;		/* payload of the packet */
	} packet_t;

	#define NAMEDPIPE_H_
#endif


