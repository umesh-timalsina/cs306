#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<getopt.h>
#define IN 1
#define OUT 0
#define MAXLINE 10000
typedef struct flags{
    unsigned int byte_flag            : 1;
    unsigned int char_flag            : 1;
    unsigned int line_flag            : 1;
    unsigned int max_line_length_flag : 1;
    unsigned int words_flag           : 1;
    unsigned int help_flag            : 1;
    unsigned int version_flag         : 1;
    char **input_file;
}flags;// defining options flag

typedef struct counts_values{
    int num_bytes;
    int num_chars;
    int num_words;
    int num_lines;
    int max_line_length;
}counts_values;
/* The main Function in the parent process */
int main(int argc, char **argv){
    /* Function prototypes to find options and file name */
    void find_option(struct flags *input_flag, 
                     int *num_files,
                     int argc, char **argv);

    void print_with_opts(struct flags *flags_input, struct counts_values *val);
    void display_version();
    void display_help();
    /* Define a structure to find the options selected */
    struct flags flag_input = {0, 0, 0, 0, 0, 0, 0, NULL};
    extern int opterr;
    opterr = 0; // disable the error flag of the header file
    struct flags *ptr;
    ptr = &flag_input;
    int num_files = 0; // the number of files to work on

    /* call the function */
    find_option(ptr, &num_files, argc, argv);
    pid_t childpid; // necessary to distinguish between parent and child process
    int pipefd1[2], pipefd2[2]; // Creating 2 pipe file descriptors
    pipe(pipefd1);
    pipe(pipefd2);
    /* Fork off a single process */
    if((childpid = fork()) == -1){
        perror("fork");
        exit(EXIT_FAILURE);
    }// end if

    if(childpid == 0){
        /* Here is the child process */
        /* Close the write end of the pipe */
        /* Count Array to implement counters*/
        close(pipefd1[1]);
        int counts[5] = {0, 0, 0, 0, 0};
        char c;
        int state = OUT;
        int max_length=0, temp_length=0;
        while(read(pipefd1[0], &c, sizeof(c)) == 1){ 
            counts[1]++; // Characters Count
            temp_length++;
            (c == '\n')? (counts[2]++): (counts[2]+=0); // lines count
            if( c == '\n' ){
                temp_length = 0;
            }// end if
            if(c == '\n' || c == ' ' || c == '\t'){
                state = OUT;  
            }// end if
            else if(state == OUT){
                state = IN;
                counts[3]++;// words count
            }// end else if
            if(temp_length > max_length){
                max_length = temp_length;
            }// end if
            counts[0]++; // bytes count
        }// end while

        counts[4] = max_length;   // longest line length
        counts[0] = counts[1];      // characters count = bytes count
        close(pipefd1[0]);          // Close the read end
     
        counts_values *struct_counts = malloc(sizeof(counts_values));
        if(struct_counts == NULL){
            fprintf(stderr, "Failed to allocate memory");
            exit(EXIT_FAILURE);
        }// end if

        struct_counts -> num_bytes = counts[0];
        struct_counts -> num_chars = counts[1];
        struct_counts -> num_lines = counts[2];
        struct_counts -> num_words = counts[3];
        struct_counts -> max_line_length = counts[4];
        
        close(pipefd2[0]);      // Close the read end of the second pipe
        write(pipefd2[1], struct_counts, sizeof(counts_values));
        close(pipefd2[1]);      // close the write end as well
        free(struct_counts); // Free the malloced memory
        exit(EXIT_SUCCESS); // exit
    }// end if

    else{
        close(pipefd1[0]);// close the read end of the pipe
        FILE *fp = fopen(flag_input.input_file[0], "r");
        if(fp == NULL){
            fprintf(stderr, "Failed to open file");
            exit(EXIT_FAILURE);
        }// end if
        char temp;
        while((temp = fgetc(fp)) != EOF){
            /* Send the contents of the file char by char 
             * to the first pipe 
             */
            write(pipefd1[1], (void *) &temp, sizeof(temp));
        }// end while
        write(pipefd1[1], (void *) '0', sizeof(temp));
        close(pipefd1[1]);
        /* Read the file in parent process */

        counts_values *struct2 = malloc(sizeof(counts_values));
        close(pipefd2[1]); // close the write end of the second pipe
        read(pipefd2[0], struct2, sizeof(counts_values));
        print_with_opts(ptr, struct2);
        close(pipefd2[0]);
        free(struct2);
        exit(EXIT_SUCCESS); // exit
    }// end else
  
}// end main



void find_option(struct flags *input_flag, 
                 int *num_files,
                 int argc,        char   **argv){
        
    int c;
    while(1){
        int option_index = 0;
        static struct option long_options[] = {
            { "bytes",           no_argument,   NULL, 'c' },
            { "chars",           no_argument,   NULL, 'm' },
            { "lines",           no_argument,   NULL, 'l' },
            { "max-line-length", no_argument,   NULL, 'L' },
            { "words",           no_argument,   NULL, 'w' },
            { "help",            no_argument,   0,     0  },
            { "version",         no_argument,   0,     0  },
            { 0,                 0,             0,     0  },
        };// end long_options
        c = getopt_long(argc, argv, "cmlLw", 
                        long_options, &option_index);
        if(c == -1){
            break;
        }// break
        switch(c){
            case 0:
                //printf("%d\n", option_index);
                break;
            case 'c':
               // printf("%c\n", c);
                input_flag->byte_flag = 1;
                break;
            case 'm':
                input_flag->char_flag = 1;
                break;
            case 'l':
                input_flag->line_flag = 1;
                break;
            case 'L':
                input_flag->max_line_length_flag = 1;
                break;
            case 'w':
                input_flag->words_flag = 1;
                break;
            case '?':
                fprintf(stderr, "%s: invalid option -- '%s'\n", argv[0], argv[(optind-1)]);
                fprintf(stderr, "Try '%s --help' for more information.\n", argv[0]);
                exit(EXIT_FAILURE);
        }// end switch
        if(option_index == 5){
            input_flag->help_flag = 1;
        }// end if
        else if(option_index == 6){
            input_flag->version_flag = 1;
        }// end else if
    }// end while
    *num_files = argc-optind;
    int i  = 0;
    input_flag->input_file = (char **)malloc(sizeof(char *) * (*num_files));
    if(input_flag->input_file == NULL){
        fprintf(stderr, "Error- Failed to allocate memory \n");
        exit(EXIT_FAILURE);
    }//end if
    if(input_flag->input_file == NULL){
        fprintf(stderr, "Error- Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    for(i = 0; i < *num_files; i++){
        input_flag->input_file[i] = argv[optind+i];
    }// end for
}// end find_option



void print_with_opts(struct flags *flags_input, struct counts_values *vals){
   char str_to_print[MAXLINE]="";
   char buf[MAXLINE]="";
//   void display_help();
   void display_version();
   if(flags_input->line_flag){
        sprintf(buf, " %d ", vals-> num_lines );
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->words_flag){
       sprintf(buf, " %d ", vals -> num_words);
       strcat(str_to_print, buf);
   }//end if
   if(flags_input->byte_flag){
        sprintf(buf, " %d ", vals -> num_bytes);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->char_flag){
        sprintf(buf, " %d ", vals -> num_chars);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->max_line_length_flag){
        sprintf(buf, " %d ", vals -> max_line_length);
        strcat(str_to_print, buf);
   }// end if
   if(flags_input->help_flag){
//        display_help();
        exit(EXIT_SUCCESS);
   }// end if
   if(flags_input->version_flag){
        // display_version(); 
        exit(EXIT_SUCCESS);
   }// end if
   if(strcmp(str_to_print, "") == 0){
        sprintf(str_to_print, " %d %d %d %s", 
                vals -> num_lines, vals -> num_words, 
                vals -> num_bytes ,flags_input->input_file[0]);
   }// end if
   else{
        strcat(str_to_print, flags_input->input_file[0]);
   }// end else
   printf("%s\n", str_to_print); 
}// end print_with_opts

void display_version(){
    printf(" mywc (Mimics wc) \n") ;
    printf("Copyright (C) 2017 Umesh Timalsina<umesh.timalsina@siu.edu> \n");
    printf("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n");
    printf("This is free software: you are free to change and redistribute it.\n");
    printf("There is NO WARRANTY, to the extent permitted by law.\n");
    printf( "Written by Umesh Timalsina \n");
}// end display_version

void display_help(){
    

  printf("usage: ./mywc [OPTION]... [FILE]...\n"
        "Print newline, word, and byte counts for a FILE.  A word is a non-zero-length sequence or\n"
         "characters delimited by white space.\n");

  printf("Some filename should be given, otherwise the program will exit by printing error message to\n"
         "stderr\n");

  printf("The options below may be used to select which counts are printed, always in\n"
           "the following order: newline, word, character, byte, maximum line length.\n"
           "-c, --bytes            print the byte counts\n"
           "-m, --chars            print the character counts\n"
           "-l, --lines            print the newline counts\n"
           "-L, --max-line-length  print the maximum display width\n"
           "-w, --words            print the word counts\n"
            "    --help             display this help and exit\n"
            "    --version          output version information and exit\n");


}// end display_help
