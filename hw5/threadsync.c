/*
 * For testing if you run ./threadsync -c 0 -i 1 -d 1 (or for 306 students if you take counter as 0, incr as 1 and decr as 1 from argc, argv),  
 * then final output should be printed out as 0. That is when your code is right.
 */


#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<assert.h>
#include<errno.h>
#include<getopt.h>
#define loops 1000000

/* This is your global variable that will be updated by multiple threads - need to synchronize, access to it */
//int counter;
int counter = 0L; // global variable counter
/* Your code goes here: initialize a mutex_lock. Remember, since the mutex lock is shared between threads,
   it MUST BE DECLARED AS A GLOBAL variable */
pthread_mutex_t cntr_mutex = PTHREAD_MUTEX_INITIALIZER;


void* increment(void *arg)
{
	/* Your code goes here: copy the value of arg to the local int variable incr_step */
	int incr_step;
    incr_step = (int) arg;
	unsigned long int i; 
    //printf("%d\n", incr_step);
	/* Your code goes here: you need to add thread synchronization statements (lock and unlock)
       for accessing the shared variable "counter" inside the loop.
       Points will be deducted, if your synchronization block covers more
       statements than absolutely necessary! Remember: since locking reduces
       concurrency, you MUST MAKE SURE that you lock only the minimum set of statements and for MINIMUM amount of time! 
    */	
	for(i=0; i<loops;i++) {
        assert(pthread_mutex_lock(&cntr_mutex) == 0);
		counter = counter + incr_step;
        assert(pthread_mutex_unlock(&cntr_mutex) == 0);
		// I have commented this line out. You can uncomment it for debugging.
	    //printf("Thread ID %d --> counter = %ld\n", pthread_self(), counter);
	}

	return NULL;	/* We MUST return NULL since thread function signature requires returning (void *) datatype */
}

void* decrement(void *arg)
{
	/* Your code goes here: copy the value of arg to the local int variable decr_step */
	int decr_step;
    decr_step = (int) arg;
    int ret;
	unsigned long int i; 
	/* Your code goes here: you need to add thread synchronization statements (lock and unlock)
       for accessing the shared variable "counter" inside the loop.

       Points will be deducted, if your synchronization block covers more
       statements than absolutely necessary! Remember: since locking reduces
       concurrency, you MUST MAKE SURE that you lock only the minimum set of statements and for MINIMUM amount of time! 
    */	
	for(i=0; i<loops;i++) {
        ret = pthread_mutex_trylock(&cntr_mutex); //lock blocks till the mutex is available but trylock doesnot
		if(ret == EBUSY){
            i--; // this iteration count shouldnot be wasted
        }// end 
        else if(ret == EINVAL){
            fprintf(stderr, "Critical Error\n");
            exit(EXIT_FAILURE);
        }// end else if
        else{
            counter = counter - decr_step; // 
            ret = pthread_mutex_unlock(&cntr_mutex); // Unlock the mutex if the previous lock was successful
            assert(ret == 0);
        }// end else

		// I have commented this line out. You can uncomment it for debugging.
		//printf("Thread ID %d --> counter = %d\n", pthread_self(), counter);
	}

	return NULL;	/* We MUST return NULL since thread function signature requires returning (void *) datatype */
}

typedef struct values{
    int inc; 
    int dec;
    int ctr;
}values;
int main(int argc, char *argv[])
{
    if(argc < 2){
       fprintf(stderr, "Please use the script properly\n");
       exit(EXIT_FAILURE);
    }// end if
    void find_populate(values *ptr, int argc, char **argv);
	//int i = 0;
	int err;
	pthread_t tid[2]; 		/* We will store the thread IDs in an array */ 
	
	int incr, decr;
	/* For CS306, your code goes here: copy argv[1] to counter, argv[2] to incr and argv[3] to decr.
       These will be the arguments that you respectively pass to the two threads you will 
       create next: increment() and decrement().

	   For CS491 ONLY: instead of using argv[1], argv[2] and argv[3], you MUST use getopt() as mentioned in the instructions
 	   to get the values of counter, incr and decr respectively. 
	*/
   
    struct values *new_values = malloc(sizeof(values));
    if(new_values == NULL){
        fprintf(stderr, "Failed to allocate memory");
        exit(EXIT_FAILURE);
    }// end if
    find_populate(new_values, argc, argv);
    incr = new_values -> inc;
    decr = new_values -> dec;
    counter = new_values -> ctr; 

    
	/* Your code goes here: create the thread with entry point increment() in tid[0]. your code
       MUST check for thread creation errors and print the apprpriate error message before aborting */ 
	err = pthread_create(&tid[0], NULL, increment, (void *) incr);
    if(err != 0){
        printf("Error creating thread %d\n", (int) tid[1]);
    }// end if

	
	/* Your code goes here: create the thread with entry point decrement() in tid[1]. your code
       MUST check for thread creation errors and print the apprpriate error message before aborting */ 
    err = pthread_create(&tid[1], NULL, decrement, (void *)decr);
    if(err != 0){
        printf("Error creating thread %d\n", (int) tid[1]);
    }// end if

	
	/* Your code goes here: Now write the necessary code for joining back the two threads */
	err = pthread_join(tid[0], NULL);
    if(err != 0){
        printf("Error joining thread %d\n", (int) tid[0]);
    }// end if
    err = pthread_join(tid[1], NULL);
    if(err != 0){
        printf("Error joining thread %d\n", (int) tid[1]);
    }// end if


	/* Your code goes here: finally, destroy the mutex you created - always remember to destroy all mutexes before exiting code! */
	err = pthread_mutex_destroy(&cntr_mutex);
    if(err != 0){
        fprintf(stderr, "Failed to destroy the mutex\n");
    }// end if
	
	// Finally let's check what the value of counter is after all that processing by the above two threads
	// For testing if you run ./threadsync -c 0 -i 1 -d 1 (or for 306 students if you take counter as 0, incr as 1 and decr as 1 from argc, argv), 
	// then this line should print out 0 as the answer. That is when your code is right.
	printf("counter = %d\n", counter);
	
	return 0;
}


void find_populate(struct values *ptr, int argc, char **argv){
    int c;

    while(1){
        int option_index = 0;
        static struct option long_options[] = {
            { "count",  required_argument,   NULL, 'c' },
            { "incr",   required_argument,   NULL, 'i' },
            { "decr",   required_argument,   NULL, 'd' },
            { 0,                 0,             0,  0}
        };// end long_options
        c = getopt_long(argc, argv, "c:i:d:", 
                        long_options, &option_index);
        if(c == -1){
            break;
        }// break
        switch(c){
            case 0:
                //printf("%d\n", option_index);
                break;
            case 'c':
               // printf("%c\n", c);
                ptr->ctr = atoi(optarg);
                break;
            case 'i':
                ptr->inc = atoi(optarg);
                break;
            case 'd':
                ptr->dec = atoi(optarg);
                break;
            case '?':
                //fprintf(stderr, "%s: invalid option -- '%s'\n", argv[0], argv[(optind-1)]);
                exit(EXIT_FAILURE);
        }// end switch
    }// end while
}// end find_populate
