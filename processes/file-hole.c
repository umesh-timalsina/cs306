#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>

char buf1[] = "abcdefghij";
char buf2[] = "ABCDEFGHIJ";
mode_t FILE_MODE = S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH;
int main(){
    int fd;

    if((fd = creat("file.hole", FILE_MODE))< 0)
        perror("Failed to open file");
    if(write(fd, buf1, 10) != 10){
        perror("Buf1 write error");
        exit(EXIT_FAILURE);
    }// failed to write
    if(lseek(fd, 16384, SEEK_SET) == -1){
        perror("lseek error");
        exit(EXIT_FAILURE);
    }// end if
    if(write(fd, buf2, 10) != 10){
        perror("Buf2 write error");
        exit(EXIT_FAILURE);
    }//end if
}// end main

