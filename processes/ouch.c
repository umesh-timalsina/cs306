#include<stdio.h>
#include<signal.h>
#include<unistd.h>

void ouch(int sig){
    printf("Ouch - I got a signal %d\n", sig);
    (void) signal(SIGINT, SIG_DFL);
}// end ouch

int main(){
    (void) signal(SIGINT, ouch);
    while(1){
        printf("Hello World");
        sleep(1);
    }// end while
}// end main
