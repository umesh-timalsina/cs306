#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#define BUFFSIZE 4096

// Main Function
int main(void){
    int n;
    char buf[BUFFSIZE];
    while((n = read(STDIN_FILENO, buf, BUFFSIZE)) > 0 )
        if(write(STDOUT_FILENO, buf, n) != n)
            perror("Write Error");
    if(n < 0)
        perror("Read Error");
    return 0;
}//end main
