#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char **argv){
    char *filename;

    if(argc != 2){
        fprintf(stderr, "usage: upper2.c file\n");
        exit(EXIT_FAILURE);
    }// end if

    filename = argv[1];     // file name
    
    if(!freopen(filename, "r", stdin)) {
        fprintf(stderr, "Could not redirect stdin from %s\n", filename);
    }// end if

    execl("./upper.o", "upper", (int *)0);

    return 0;
}// end main

