#include<pthread.h>
#include<stdio.h>

void *myThread(void *arg){
    printf("Thread %d started\n", (int) arg);
    pthread_exit(arg);
}// end myThread

#define MAX_THREADS 5

int main(){
    int ret, i, status;
    pthread_t thread_ids[MAX_THREADS];
    for(i = 0; i < MAX_THREADS; i++){
        ret = pthread_create(&thread_ids[i], NULL, myThread, (void *) &i);
        if(ret != 0){
            fprintf(stderr, "Error Creating thread %d\n", (int)thread_ids[i]);
        }// end if
    }// end for

    for(i = 0; i < MAX_THREADS; i++){
        ret = pthread_join(thread_ids[i], (void **) &status);
        if(ret != 0){
            printf("Error Joining Thread %d\n", (int) thread_ids[i]);
        }// end if
        else{
            printf("Status = %d\n", status);
        }// end else
    }// end for

    return 0;
}// end main

