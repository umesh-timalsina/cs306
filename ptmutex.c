#include<pthread.h>
#include<stdio.h>
#include<assert.h>
pthread_mutex_t cntr_mutex = PTHREAD_MUTEX_INITIALIZER;
long protVariable = 0L;

void *myThread(void *arg){
    int i, ret;
    for(i = 0; i < 10001; i++){
        ret = pthread_mutex_lock(&cntr_mutex);
        assert(ret == 0);
        ++protVariable;
        ret = pthread_mutex_unlock(&cntr_mutex);
        assert(ret == 0);
    }// end for
    pthread_exit(NULL);
}// end myThread

#define MAX_THREADS 10
int main(){
    int ret, i;
    pthread_t thread_ids[MAX_THREADS];
    for(i=0; i< MAX_THREADS; i++){
        ret = pthread_create(&thread_ids[i], NULL, myThread, NULL);
        if(ret != 0){
            printf("Error Creating thread %d\n", (int) thread_ids[i]);
        }// end if
    }// end for

    for(i=0; i<MAX_THREADS; i++){
        ret = pthread_join(thread_ids[i], NULL);
        if(ret != 0){
            printf("Error Joining Thread %d\n", (int) thread_ids[i]);
        }// end if
    }// end for

    printf("The protected variable's value is %ld\n", protVariable);

    ret = pthread_mutex_destroy(&cntr_mutex);

    if(ret != 0){
        printf("Couldnot destroy mutex\n");
    }// end if
    return 0;
}
