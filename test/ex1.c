#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#define MAXLEN 40
int main(int argc, char **argv){
    if(argc != 2){
        fprintf(stderr,"Arguments error\n");
        exit(EXIT_FAILURE);
   }// end if

    int number_childs = atoi(argv[1]);
    int i;
    int status_loc;
    pid_t temp;
    int fd;
    char log_file[MAXLEN];
    pid_t child_pids[number_childs];
    for(i = 0; i<number_childs; i++){
        temp = fork();
        if(temp == -1){
            perror("Failed to fork\n");
            exit(EXIT_FAILURE);
        }// end if
        
        if(temp == 0){
            printf("I am child with pid %d\n", getpid());
            break;
        }// end if

        else{
            child_pids[i] = temp;
        }// end else

    }// end for
    if(temp == 0){
        char *argv_exec[] = {"wc", "-lw", "/usr/share/dict/words", 0};
        sprintf(log_file, "childlog_%d", getpid());
        //printf("%s\n", log_file);
        mode_t fmode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
        int foptions = O_CREAT | O_WRONLY;
        fd = open(log_file, foptions, fmode);
        close(1);
        dup(fd);
        execvp("wc", argv_exec);


    }// end if
    else{
        for(i = 0; i < number_childs; i++){
            while(waitpid(child_pids[i],&status_loc, WNOHANG) == 0);
            if(WIFEXITED(status_loc)){
                printf("Process %d exited with status %d", child_pids[i],WEXITSTATUS(status_loc));
            }// end if
            else{
                printf("process %d exited abnormally\n", child_pids[i]);
            }// end else
        }// end for
    }// end else

}// end main
