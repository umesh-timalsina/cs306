#include<stdio.h>
#include<pthread.h>

void *busy(void *ptr){
    puts((char *) ptr);
    return "Hello";
}// end busy

int main(){
    pthread_t id;
    pthread_create(&id, NULL, busy, "Hi");
    
    void *result;
    
    pthread_join(id, &result);
    printf("Thread %x joined and returned %s", (int) id,(char*) result);
    
    return 0;
}// end main
